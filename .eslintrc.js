module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  env: {
    browser: true,
    es2021: true,
    node: true
  },
  plugins: ['svelte3', 'prettier'],
  extends: [
    // add more generic rulesets here, such as:
    // 'eslint:recommended',
    'standard',
    "prettier",
  ],
  overrides: [
    {
      files: ['**/*.svelte'],
      processor: 'svelte3/svelte3',
    },
  ],
}