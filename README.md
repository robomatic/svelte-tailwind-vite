SVTWVI - Svelte / Tailwind / Vite Template
=====================================================
Project based on https://github.com/srmullen/sttv

Get up and running with Svelte, Tailwind and Vite. Also includes testing setup using Jest.

Installation
------------

```
npx degit "https://gitlab.com/robomatic/svelte-tailwind-vite" my_app
cd my_app
npm install
```

Scripts
-------

### Start a development server

`npm run dev`

### Run tests

`npm run test`
or
`npm run test:watch`

### Build the application

`npm run build`

### Serve the application

`npm run serve`